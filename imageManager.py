import sys, os, glob, random
import numpy as np
from random import randint
from PyQt4 import QtGui
from PyQt4.QtGui import QToolBar, QMessageBox, QIcon, QFileSystemModel, QTreeView, QFrame, QImage, QLabel, \
    QTreeWidgetItem, QTreeWidget, QPushButton
from PyQt4 import QtCore
from PyQt4.QtCore import Qt, QSize, QDir, QModelIndex
from iptcinfo3 import IPTCInfo

import imageOrganizer.imageOrganizer
from functools import partial

class Window(QtGui.QMainWindow):

    def __init__(self):

        super(Window, self).__init__()


        self.initializeWindow()

        self.createLateralBar()

        self.show()

    def show_favorites_panel(self):
        self.createImageViewerPanel()
        self.properties_panel = QFrame(self)
        self.properties_panel.setGeometry(self.images_panel.x() + self.images_panel.width(), 0,self.width() - self.images_panel.x() + self.images_panel.width(),self.height())
        self.properties_panel.setStyleSheet("QFrame { background-color: rgb(200, 255, 200);}")
        position_y = 200
        lbl1 = QLabel('Favorite Folders:', self.properties_panel)
        lbl1.setGeometry(10, position_y-50, self.properties_panel.width(), 60)
        try:
            folders = np.load('favorite_folders.npy')
        except IOError:
            folders = []
            np.save('favorite_folders', folders)
        for f in folders:
            position_y += 40
            lbl1 = QLabel(f, self.properties_panel)
            lbl1.setGeometry(10, position_y, self.properties_panel.width(), 60)


    def open_import_workspace(self):
        self.workspace = 'import'
        self.createFilesSystemPanel()
        self.show_favorites_panel()

    def add_favorite(self, folder):
        try:
            folders = np.load('favorite_folders.npy')
        except IOError:
            folders = []
            np.save('favorite_folders', folders)
        print(folders)
        exists = False
        for f in folders:
            if f == folder:
                exists = True
        if not exists:
            folders = np.append(folder,folders)
            np.save('favorite_folders',folders)
        for f in folders:
            print('... '+ f)

    def print_properties(self,full_path):
        print('printing iptc of',full_path)
        info = IPTCInfo(full_path, force=True)
        info.save()
        info = IPTCInfo(full_path)
        print(info['keywords'])
        b = ['passarinho','teste']
        info['keywords'] = b
        info.save()

    def create_properties_panel(self, path):
        self.properties_panel = QFrame(self)
        self.properties_panel.setGeometry(self.images_panel.x()+5+self.images_panel.width(),0,self.width()-self.images_panel.x()+self.images_panel.width()-5,self.height())
        self.properties_panel.setStyleSheet("QFrame { background-color: rgb(255, 255, 255);}")
        self.show_single_image(path, self.properties_panel)
        name_path = path.split('/').pop()
        pasta = path[:-1+path.find(name_path)]
        position_y = 200
        button = QPushButton('Favorite Folder', self.properties_panel)
        button.setGeometry(10, position_y, 110, 30)
        button.clicked.connect(partial(self.add_favorite,pasta))
        position_y += 40
        folder_path = QLabel(pasta,self.properties_panel)
        folder_path.setGeometry(10,position_y,self.properties_panel.width(),60)
        position_y+=40
        file_name = QLabel(name_path,self.properties_panel)
        file_name.setGeometry(10,position_y,self.properties_panel.width(),30)
        position_y += 40
        button_2 = QPushButton('Print IPTC', self.properties_panel)
        button_2.setGeometry(10, position_y, 110, 30)
        button_2.clicked.connect(partial(self.print_properties, path))
        self.properties_panel.show()

    def open_image(self, path):
        self.create_properties_panel(path)

    def list_images(self, path):
        self.createImageViewerPanel()
        files = glob.glob(path+'/*.*')
        for file in files:
            if self.is_image(file):
                self.show_single_image(file,self.images_panel, randint(10,self.images_panel.width()-210 ), randint(10,self.images_panel.height()-160 ))


    def show_single_image(self, path, parent, pos_x=10, pos_y =10):
        self.single_image = QLabel(parent)
        self.single_image.setGeometry(pos_x, pos_y, 200, 150)
        self.single_image.mouseReleaseEvent =lambda event:self.open_image(path)
        pixmap = QtGui.QPixmap(path)
        self.single_image.setPixmap(pixmap.scaled(200, 150))
        self.single_image.show()

    def is_image(self,path):
        path = path.lower()
        extensions = {".jpg",".jpeg", ".png", ".gif"}
        for ext in extensions:
            if path.endswith(ext):
                return True
        return False


    def select_file_item(self, index):
        path = self.sender().model().filePath(index)
        if os.path.isfile(path):
            if self.is_image(path):
                if self.workspace == 'import':
                    print('do nothing')
                else:
                    self.show_single_image(path, self.images_panel)
            else:
                print('file is not an image')
        elif os.path.isdir(path):
            if self.workspace == 'import':
                self.origin_folder = path
                print('origin='+path)
            else:
                self.list_images(path)
                print('listing ' + path)
        else:
            print('invalid')


    def createImageViewerPanel(self):
        self.images_panel = QFrame(self)
        self.images_panel.setGeometry(45+350, 0, 700, self.height())
        self.images_panel.setStyleSheet("QFrame { background-color: rgb(255, 255, 255);}")
        self.images_panel.show()


    def createFilesSystemPanel(self):
        files_panel = QFrame(self)
        files_panel.setGeometry(45, 0, 350, self.height()+26)
        files_panel.setStyleSheet("QFrame { background-color: rgb(255, 255, 255);}")
        model = QFileSystemModel()
        model.setRootPath(QDir.homePath())
        tree = QTreeView(files_panel)
        tree.setModel(model)
        tree.setRootIndex(model.index(QDir.homePath()))
        tree.setGeometry(0, -26, files_panel.width(), files_panel.height())
        tree.setColumnWidth(0, files_panel.width())
        tree.hideColumn(4)
        tree.hideColumn(3)
        tree.hideColumn(2)
        tree.hideColumn(1)
        tree.clicked.connect(self.select_file_item)
        files_panel.show()


    def openEditor(self):
        self.createFilesSystemPanel()
        self.createImageViewerPanel()


    def showToast(self):
        messagebox = QMessageBox()
        messagebox.setIcon(QMessageBox.Information)
        messagebox.setText("Not Implemented")
        messagebox.exec_()
        messagebox.close()


    def doSomething(self):
        self.showToast()


    def setLateralBarActions(self):
        self.barIcon = QtGui.QIcon()
        self.barIcon.addFile("assets/graph.png", QSize(2,2), QIcon.Normal, QIcon.On)
        self.action = QtGui.QAction(self.barIcon, "Explore and edit images", self)
        self.action.triggered.connect(self.openEditor)
        self.toolBar.addAction(self.action)
        self.action = QtGui.QAction(QtGui.QIcon('assets/star.png'), "Show favorite images", self)
        self.action.triggered.connect(self.doSomething)
        self.toolBar.addAction(self.action)
        self.action = QtGui.QAction(QtGui.QIcon('assets/robot.png'), "Show favorite images", self)
        self.action.triggered.connect(self.open_import_workspace)
        self.toolBar.addAction(self.action)

    def createLateralBar(self):
        # Lateral Bar which changes the workspace
        self.toolBar = QToolBar()
        self.toolBar.setFloatable(False)
        self.toolBar.setMovable(False)
        self.toolBar.setStyleSheet("QToolBar { background-color: rgb(255, 255, 255) }")
        self.setLateralBarActions()
        self.windowToolBar = self.addToolBar(Qt.LeftToolBarArea, self.toolBar)


    def initializeWindow(self):
        self.setWindowTitle("Image Manager")
        self.setWindowIcon(QtGui.QIcon('assets/application_icon.png'))
        self.workspace = 'none'
        self.setWindowState(QtCore.Qt.WindowMaximized)

def run():
    app = QtGui.QApplication(sys.argv)
    window = Window()
    sys.exit(app.exec_())


if __name__ == "__main__":
    run()
