import tensorflow as ts
from keras.models import Sequential
from keras.layers import Dense


def normalize():
    print('normalizing data')


def run(images=''):
    print('Creating the model, images =', images)

    model = Sequential()
    model.add(Dense(1024, input_shape=(32, 32), activation='relu'))
    model.add(Dense(1024, activation='relu'))
    model.add(Dense(10))
    model.compile(optimizer='adam', loss='mse')

    print('Finish compile model')


if __name__ == "__main__":
    run('cool')
